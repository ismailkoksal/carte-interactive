// On initialise la latitude et la longitude de Ploufragan (centre de la carte)
let lat = 48.4926;
var lon = -2.79421;
var macarte = null;

// Fonction d'initialisation de la carte
function initMap() {
    // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
    macarte = L.map('map').setView([lat, lon], 15);
    // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
        // Il est toujours bien de laisser le lien vers la source des données
        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(macarte);

    // Nous appelons la fonction ajax de jQuery
    $.ajax({
        // On pointe vers le fichier selectData.php
        url: "selectDataBorne.php",
    }).done(function (json) { // Si on obtient une réponse, elle est stockée dans la variable json
        // create custom icon
        var homeIcon = L.icon({
            iconUrl: 'https://image.flaticon.com/icons/svg/854/854923.svg',
            iconSize: [38, 95], // size of the icon
        });

        // On construit l'objet borne à partir de la variable json
        var borne = JSON.parse(json);
        // On parcourt l'objet borne
        for (var b in borne) {
            L.marker([parseFloat(borne[b].latitudeB), parseFloat(borne[b].longitudeB)], {icon: homeIcon}).addTo(macarte)
                .bindPopup(borne[b].nomB)
                .openPopup();
        }
    });
    $.ajax({
        // On pointe vers le fichier selectData.php
        url: "selectDataVelo.php",
    }).done(function (json) { // Si on obtient une réponse, elle est stockée dans la variable json
        var bicycleIcon = L.icon({
            iconUrl: 'https://image.flaticon.com/icons/svg/923/923770.svg',
            iconSize: [38, 95], // size of the icon
        });

        // On construit l'objet velo à partir de la variable json
        var velo = JSON.parse(json);
        // On parcourt l'objet borne
        for (var v in velo) {
            L.marker([parseFloat(velo[v].latitudeV), parseFloat(velo[v].longitudeV)], {icon: bicycleIcon}).addTo(macarte);
        }
    });

}

window.onload = function () {
    // Fonction d'initialisation qui s'exécute lorsque le DOM est chargé
    initMap();
};